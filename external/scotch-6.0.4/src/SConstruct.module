#import os
#import sys
#import platform
#import subprocess
#import re

from Caelus.build import *
import Caelus

#===============================================================================
# Scotch variables
#===============================================================================
globalEnv.Replace(BIN_PLATFORM_INSTALL = os.path.join(Caelus.SCOTCH_PATH, 'bin'))
globalEnv.Replace(LIB_PLATFORM_INSTALL = os.path.join(Caelus.SCOTCH_PATH, 'lib'))
globalEnv.Replace(LIB_SRC = os.path.join(Caelus.SCOTCH_PATH, 'src'))
globalEnv.Replace(PROJECT_INC = os.path.join(Caelus.SCOTCH_PATH, 'include'))
globalEnv.Replace(CPPPATH = globalEnv['PROJECT_INC'])

#===============================================================================
# Scotch compiler flags
#===============================================================================
if Caelus.WHICH_OS == "windows" :
   # General compiler flags
   GFLAGS = '-DCOMMON_RANDOM_FIXED_SEED'  \
   + ' -DCOMMON_STUB_FORK -DSCOTCH_RENAME ' \
   + ' -DCOMMON_RANDOM_RAND -DSCOTCH_PTSCOTCH '  \
   + ' -Dwindows' \
   + ' -DCOMMON_WINDOWS -DCOMMON_FILE_COMPRESS_GZ '
#end of windows compiler flags

elif Caelus.WHICH_OS == "darwin" :
   # Set the C++ compiler
   globalEnv.Replace(CXX = globalEnv['COMPILER'])
   if globalEnv['COMPILER'] == 'clang++':
      globalEnv.Replace(CC = 'clang')
  
   # General compiler flags
   GFLAGS = '-O3' + ' -DCOMMON_FILE_COMPRESS_GZ -DCOMMON_PTHREAD -DCOMMON_PTHREAD_BARRIER -DCOMMON_RANDOM_FIXED_SEED  -DCOMMON_TIMING_OLD -DSCOTCH_RENAME -DSCOTCH_PTSCOTCH -Wno-switch -Wno-implicit-int -Wno-empty-body -Wno-format-security'
#end of macosx compiler flags

else :
  # General compiler flags
  GFLAGS = '-O3' + ' -DCOMMON_FILE_COMPRESS_GZ -DCOMMON_PTHREAD -DCOMMON_RANDOM_FIXED_SEED ' \
  + '-DSCOTCH_RENAME -DSCOTCH_PTSCOTCH '

  # Flag to turn label into 64bit int during compilation
  if  globalEnv['INT_TYPE'] == '64':
      GFLAGS = GFLAGS + ' -DCAELUS_LABEL64'

#end of linux compiler flags

# Setup full compiler flags
# override existing flags???
globalEnv.Replace(CCFLAGS = GFLAGS)

#===============================================================================
# Extra include path(s)
#===============================================================================
if Caelus.WHICH_OS == "windows":
   globalEnv.Append(CPPPATH = [os.path.join(Caelus.ZLIB_PATH, 'include')])

#===============================================================================
# Extra linker flags
#===============================================================================
if Caelus.WHICH_OS == "darwin":
   globalEnv.Append(LINKFLAGS = '-dynamiclib -undefined dynamic_lookup')

#===============================================================================
# Extra link library path(s)
#===============================================================================
if Caelus.WHICH_OS == "windows" :
  globalEnv.Append(LIBPATH = [globalEnv['LIB_PLATFORM_INSTALL'], os.path.join(globalEnv['ZLIB_PATH'], 'lib'),
  globalEnv['MPI_LIB']])
else:
   globalEnv.Append(LIBPATH = [globalEnv['LIB_PLATFORM_INSTALL']])

#===============================================================================
# Extra link library(ies)
#===============================================================================
if Caelus.WHICH_OS == "windows" :
  lib_link = ['z', globalEnv['MPI_LIB_NAME']]
  globalEnv.Append(LIBS =lib_link)

elif Caelus.WHICH_OS == "darwin" :
  globalEnv.Append(LIBS ='')

else :
  globalEnv.Append(LIBS ='rt')

#===============================================================================
# Sub-directories to traverse for compile
#===============================================================================
subdirs = [
   'libptscotcherr',
   'libptscotcherrexit',
   'libptscotch'
]

#===============================================================================
# Execute SConscript files in each sub-directory
#===============================================================================
for dir in subdirs:
  if GetOption('clean'):
    SConscript( os.path.join(dir, 'SConscript'), exports = ['globalEnv'])
	
  else:
    SConscript( os.path.join(dir, 'SConscript'), exports = ['globalEnv'])

#===============================================================================
# That's All Folks!
#===============================================================================
