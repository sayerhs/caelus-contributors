#Copyright (C) 2014 Applied CCM
import os
import sys
import platform
import subprocess
import re

#===============================================================================
# Define colours for pretty output (non-Windows only)
#===============================================================================
colors = {}
colors['cyan']   = '\033[1;36m'
colors['purple'] = '\033[1;35m'
colors['blue']   = '\033[1;34m'
colors['green']  = '\033[1;32m'
colors['yellow'] = '\033[1;33m'
colors['red']    = '\033[0;31m'
colors['end']    = '\033[0m'
colors['white']  = '\033[1;37m'
colors['bluebg']  = '\033[44;37m'

#If the output is not a terminal, remove the colors
if not sys.stdout.isatty():
 for key, value in colors.iteritems():
    colors[key] = ''

compile_source_message = '%sCompiling %s==> %s$SOURCE%s' % \
 (colors['blue'], colors['purple'], colors['yellow'], colors['end'])

compile_shared_source_message = '%sCompiling shared %s==> %s$SOURCE%s' % \
 (colors['blue'], colors['purple'], colors['yellow'], colors['end'])

link_program_message = '%sLinking Program %s==> %s$TARGET%s' % \
 (colors['red'], colors['purple'], colors['yellow'], colors['end'])

link_library_message = '%sLinking Static Library %s==> %s$TARGET%s' % \
 (colors['red'], colors['purple'], colors['yellow'], colors['end'])

ranlib_library_message = '%sRanlib Library %s==> %s$TARGET%s' % \
 (colors['red'], colors['purple'], colors['yellow'], colors['end'])

link_shared_library_message = '%sLinking Shared Library %s==> %s$TARGET%s' % \
 (colors['red'], colors['purple'], colors['yellow'], colors['end'])

java_library_message = '%sCreating Java Archive %s==> %s$TARGET%s' % \
 (colors['red'], colors['purple'], colors['yellow'], colors['end'])
 
install_library_message = '%sInstalling Library %s==> %s$TARGET%s' % \
 (colors['green'], colors['purple'], colors['yellow'], colors['end'])

#===============================================================================
# Pass operating system envrionment
#===============================================================================

# Just grab the type of operating system
which_os = os.environ['WHICH_OS']
    
# Default Global Environment is dependent on the tpye of operating system
if which_os == 'windows' :
  globalEnv = Environment(tools = ['mingw'], ENV = os.environ)
  globalEnv.Append(OSTYPE = 'windows')
else:
  globalEnv = Environment(ENV = os.environ)
  globalEnv.Append(OSTYPE = os.name) 

# Slash for paths
sl = globalEnv['ENV']['SLASH']


#===============================================================================
# Common environment variables
#===============================================================================
# The OS enviornment is placed inside the ENV dictionary, we don't want to index 
# into this everytime so we now create local copies of the variables
globalEnv.Append(SLASH = globalEnv['ENV']['SLASH'])
globalEnv.Append(WHICH_OS = globalEnv['ENV']['WHICH_OS'])
globalEnv.Append(COLOUR_BUILD = globalEnv['ENV']['COLOUR_BUILD'])
globalEnv.Append(COMPILER_ARCH = globalEnv['ENV']['COMPILER_ARCH'])
globalEnv.Append(COMPILER = globalEnv['ENV']['COMPILER'])
globalEnv.Append(BUILD_TYPE = globalEnv['ENV']['BUILD_TYPE'])
globalEnv.Append(INT_TYPE = globalEnv['ENV']['INT_TYPE'])
globalEnv.Append(PRECISION_OPTION = globalEnv['ENV']['PRECISION_OPTION'])
globalEnv.Append(PROJECT = globalEnv['ENV']['PROJECT'])
globalEnv.Append(PROJECT_VER = globalEnv['ENV']['PROJECT_VER'])
globalEnv.Append(CAELUS_PROJECT_DIR=globalEnv['ENV']['CAELUS_PROJECT_DIR'])
globalEnv.Append(EXTERNAL_DIR = globalEnv['ENV']['EXTERNAL_DIR'])
globalEnv.Append(BUILD_OPTION = globalEnv['ENV']['BUILD_OPTION'])
globalEnv.Append(MPI_LIB = globalEnv['ENV']['MPI_LIB'])
globalEnv.Append(MPI_INC = globalEnv['ENV']['MPI_INC'])
globalEnv.Append(MPI_LIB_NAME = globalEnv['ENV']['MPI_LIB_NAME'])
globalEnv.Append(ZLIB_PATH = globalEnv['ENV']['ZLIB_PATH'])
globalEnv.Append(SCOTCH_PATH = globalEnv['ENV']['SCOTCH_PATH'])
globalEnv.Append(LIB_SRC = os.path.join(globalEnv['SCOTCH_PATH'], 'src'))
globalEnv.Append(BIN_PLATFORM_INSTALL = os.path.join(globalEnv['SCOTCH_PATH'], 'bin'))
globalEnv.Append(LIB_PLATFORM_INSTALL = os.path.join(globalEnv['SCOTCH_PATH'], 'lib'))
globalEnv.Append(PROJECT_INC = [os.path.join(globalEnv['ZLIB_PATH'], 'include'), os.path.join(globalEnv['SCOTCH_PATH'], 'include')])
#===============================================================================
# Add colourful output
if (globalEnv['COLOUR_BUILD'] == "on"):
  globalEnv.Append(CXXCOMSTR = compile_source_message)
  globalEnv.Append(CCCOMSTR = compile_source_message)
  globalEnv.Append(SHCCCOMSTR = compile_shared_source_message)
  globalEnv.Append(SHCXXCOMSTR = compile_shared_source_message)
  globalEnv.Append(ARCOMSTR = link_library_message)
  globalEnv.Append(RANLIBCOMSTR = ranlib_library_message)
  globalEnv.Append(SHLINKCOMSTR = link_shared_library_message)
  globalEnv.Append(LINKCOMSTR = link_program_message)
  globalEnv.Append(JARCOMSTR = java_library_message)
  globalEnv.Append(JAVACCOMSTR = compile_source_message)
  globalEnv.Append(INSTALLSTR = install_library_message)

# Output system architecture        
if platform.machine().endswith('64'):
  platform_arch = '64bit'    
  print ">> Architecture : " + platform_arch
else:
  platform_arch = '32bit'
  print ">> Architecture : " + platform_arch

# Warn user if requested compiler architecture is different from system
if platform_arch != globalEnv['COMPILER_ARCH']+'bit' :
  print '! You have request a ', globalEnv['COMPILER_ARCH']+'bit','build on a',\
        platform_arch, 'system, this may not be possible'

# build architecture
print ">> Build architecture : " + globalEnv['COMPILER_ARCH']+ 'bit'

# build precision
print ">> Build precision : " + globalEnv['PRECISION_OPTION'] 

# integer type
print ">> Integer type : " + globalEnv['INT_TYPE'] + 'bit'

#===============================================================================
# Common compiler flags
#===============================================================================
if which_os == "windows" :
  # General compiler flags
  GFLAGS = '-DCOMMON_RANDOM_FIXED_SEED'  \
  + ' -DCOMMON_STUB_FORK -DSCOTCH_RENAME  ' \
  + ' -DCOMMON_RANDOM_RAND -DSCOTCH_PTSCOTCH'  \
  + ' -Dwindows' \
  + ' -DCOMMON_WINDOWS -DCOMMON_FILE_COMPRESS_GZ'

#end of windows compiler flags

elif which_os == "darwin" :
  # Set the C++ compiler
  globalEnv.Replace(CXX = globalEnv['COMPILER'])
  if globalEnv['COMPILER'] == 'clang++':
    globalEnv.Replace(CC = 'clang')
  
  # General compiler flags
  GFLAGS = '-O3' + ' -DCOMMON_FILE_COMPRESS_GZ -DCOMMON_PTHREAD ' \
  + '-DCOMMON_PTHREAD_BARRIER -DCOMMON_RANDOM_FIXED_SEED  -DCOMMON_TIMING_OLD ' \
  + '-DSCOTCH_RENAME -DSCOTCH_PTSCOTCH '  \
  + '-Wno-switch -Wno-implicit-int -Wno-empty-body -Wno-format-security'
#end of macosx compiler flags

else :
  # General compiler flags
  GFLAGS = '-O3' + ' -DCOMMON_FILE_COMPRESS_GZ -DCOMMON_PTHREAD -DCOMMON_RANDOM_FIXED_SEED ' \
  + '-DSCOTCH_RENAME -DSCOTCH_PTSCOTCH'

  # Flag to turn label into 64bit int during compilation
  if  globalEnv['INT_TYPE'] == '64':
    GFLAGS = GFLAGS + ' -DCAELUS_LABEL64'

#end of linux compiler flags

# Setup full compiler flags
globalEnv.Append(CCFLAGS = GFLAGS)

#===============================================================================
# Common include path(s)
#===============================================================================
if which_os == "windows" :
  globalEnv.Append(CPPPATH = [globalEnv['PROJECT_INC'], os.path.join(globalEnv['ZLIB_PATH'], 'include'),
  os.path.join(globalEnv['SCOTCH_PATH'], 'src/misc')])

elif which_os == "darwin" :
  globalEnv.Append(CPPPATH = [globalEnv['PROJECT_INC']])

else :
  globalEnv.Append(CPPPATH = [globalEnv['PROJECT_INC']])

#===============================================================================
# Common linker flags
#===============================================================================
if which_os == "windows" :
  globalEnv.Prepend(LINKFLAGS = '')

elif which_os == "darwin" :
  globalEnv.Prepend(LINKFLAGS = '-dynamiclib -undefined dynamic_lookup' )

else :
  globalEnv.Prepend(LINKFLAGS = '')

#===============================================================================
# Common link library path(s)
#===============================================================================
if which_os == "windows" :
  globalEnv.Append(LIBPATH = [globalEnv['LIB_PLATFORM_INSTALL'], os.path.join(globalEnv['ZLIB_PATH'], 'lib'),
  globalEnv['MPI_LIB']])
else:
  globalEnv.Append(LIBPATH = globalEnv['LIB_PLATFORM_INSTALL'])

#===============================================================================
# Common link library(ies)
#===============================================================================
if which_os == "windows" :
  lib_link = ['z', globalEnv['MPI_LIB_NAME']]
  globalEnv.Append(LIBS =lib_link)

elif which_os == "darwin" :
  globalEnv.Append(LIBS ='')

else :
  globalEnv.Append(LIBS ='rt')

#===============================================================================
# Sub-directories to traverse for compile
#===============================================================================
subdirs = [
  'libptscotcherr',
  'libptscotcherrexit',
  'libptscotch'
]

#===============================================================================
# Execute SConscript files in each sub-directory
#===============================================================================
for dir in subdirs:
  if GetOption('clean'):	
    SConscript( os.path.join(dir, 'SConscript'), exports = ['globalEnv'])

  else:
    SConscript( os.path.join(dir, 'SConscript'), exports = ['globalEnv'])
#===============================================================================
# That's All Folks!
#===============================================================================
