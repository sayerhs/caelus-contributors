/*---------------------------------------------------------------------------*\
Copyright (C) 2011 OpenFOAM Foundation
-------------------------------------------------------------------------------
License
    This file is part of CAELUS.

    CAELUS is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CAELUS is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with CAELUS.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

inline CML::scalar CML::liquidProperties::W() const
{
    return W_;
}


inline CML::scalar CML::liquidProperties::Tc() const
{
    return Tc_;
}


inline CML::scalar CML::liquidProperties::Pc() const
{
    return Pc_;
}


inline CML::scalar CML::liquidProperties::Vc() const
{
    return Vc_;
}


inline CML::scalar CML::liquidProperties::Zc() const
{
    return Zc_;
}


inline CML::scalar CML::liquidProperties::Tt() const
{
    return Tt_;
}


inline CML::scalar CML::liquidProperties::Pt() const
{
    return Pt_;
}


inline CML::scalar CML::liquidProperties::Tb() const
{
    return Tb_;
}


inline CML::scalar CML::liquidProperties::dipm() const
{
    return dipm_;
}


inline CML::scalar CML::liquidProperties::omega() const
{
    return omega_;
}


inline CML::scalar CML::liquidProperties::delta() const
{
    return delta_;
}


// ************************************************************************* //
